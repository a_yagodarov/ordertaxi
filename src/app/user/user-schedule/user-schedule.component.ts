import { Component, OnInit } from '@angular/core';
import { ScheduleTemplate} from '../../_models/schedule.template';
import { ActivatedRoute, Router } from '@angular/router';
import { Schedule } from '../../_models/schedule';
import { UserScheduleService } from '../../_services/user.schedule.service';
declare var $: any;
declare var moment: any;

@Component({	
	moduleId: module.id.toString(),
	templateUrl: './user-schedule.component.html',
	styleUrls: ['./user-schedule.component.css']
})
export class UserScheduleComponent implements OnInit {
	selectedDate: any;
	id : any;
	model = new Schedule;
	errors: any = [];
	modalHeader : String;
	modalAction : String;
	event : any;
	ngOnInit() {
		this.route.data
		.subscribe(data => {
			var containerEl = $('#fullCalendar');

		    containerEl.fullCalendar({
		    	locale: 'ru',
		    	displayEventTime: false,
				dayClick: (date, jsEvent, view) => {
					this.initModalDayClick(date, jsEvent, view);
				},
				eventClick: (calEvent, jsEvent, view) => {
					this.event = calEvent;
					this.initModalEventClick(calEvent, jsEvent, view);
				}
		    });

			var models = data['data']['models'];

			for(let model of models)
			{
				var title = moment(model['date_start']).format('HH:mm')+' - '+moment(model['date_end']).format('HH:mm');
				var event = { id: model['id'] , title: title, start:  moment(model['date_start']).toDate(), model : model};
				$('#fullCalendar').fullCalendar('renderEvent', event, true);
			}
	    });
  	}

  	initModalDayClick(date, jsEvent, view)
  	{
  		this.errors = [];

		this.modalHeader = "Добавление";
		this.modalAction = "create";
		this.selectedDate = moment(date).format('DD.MM.YYYY');

		this.model.date_start = "00:00";
		this.model.date_end = "23:59";
		this.model.id = null;

		$('#myModal').modal();
  	}

  	modalDelete()
  	{
  		this.service.delete(this.model.id).subscribe(data => {
  			$('#fullCalendar').fullCalendar('removeEvents', this.model.id);
  			$('#myModal').modal('toggle');
  		});
  	}
  	modalSubmit()
  	{
  		this.errors = [];
		let submitModel = Object.assign({}, this.model);

		submitModel.date_start =  moment(this.selectedDate, "DD.MM.YYYY").set({
			h : this.model['date_start'].split(":")[0],
			m : this.model['date_start'].split(":")[1],
		}).format();
		submitModel.date_end = moment(this.selectedDate, "DD.MM.YYYY").set({
			h : this.model['date_end'].split(":")[0],
			m : this.model['date_end'].split(":")[1],
		}).format();
		submitModel.user_id = this.id;
		console.log(submitModel);
  		if (this.model.id)
  		{
  			this.service.update(submitModel).subscribe(data => {
	  			var title = moment(submitModel['date_start']).format('HH:mm')+' - '+moment(submitModel['date_end']).format('HH:mm');
	  			// var event = { id: submitModel['id'] , title: title, start:  moment(submitModel['date_start']).toDate(), model : submitModel};
	  			this.event.title =  title;
	  			this.event.start =  moment(submitModel['date_start']).toDate();
	  			this.event.model =  submitModel;
				$('#fullCalendar').fullCalendar('updateEvent', this.event, true);
				$('#myModal').modal('toggle');
	  		}, error => {
	  			this.errors = error['error'];
	  		});
  		}
  		else
  		{
  			this.service.create(submitModel).subscribe(data => {
  				console.log(data);
	  			var title = moment(submitModel['date_start']).format('HH:mm')+' - '+moment(submitModel['date_end']).format('HH:mm');
	  			var event = { id: data['id'] , title: title, start:  moment(submitModel['date_start']).toDate(), model : data};
				$('#fullCalendar').fullCalendar('renderEvent', event, true);
				$('#myModal').modal('toggle');
	  		}, error => {
	  			this.errors = error['error'];
	  		});
  		}
  	}

  	initModalEventClick(calEvent, jsEvent, view)
  	{
  		this.errors = [];

  		this.modalHeader = "Редактирование";
  		this.modalAction = "edit";
		this.selectedDate = moment(calEvent.start).format('DD.MM.YYYY');

		this.model = Object.assign({}, calEvent.model);
		this.model.id = calEvent.model.id;
		this.model.date_start = moment(calEvent.model.date_start).format('HH:mm');
		this.model.date_end = moment(calEvent.model.date_end).format('HH:mm');
		console.log(calEvent);

		$('#myModal').modal();
  	}

	constructor(private route:ActivatedRoute, private service: UserScheduleService) {
		this.id = +route['_futureSnapshot']['_urlSegment']['segments'][1]['path'];
	}
}
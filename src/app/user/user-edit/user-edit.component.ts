import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../_models/user';
import { UserService } from "../../_services/user.service";
import { StorageHelper } from "../../_helpers/storage.helper";

@Component({
  moduleId: module.id.toString(),
  templateUrl: './user-edit.component.html',
})
export class UserEditComponent implements OnInit {
	model = new User;
	errors: any = [];
	success = false;
	constructor(
		private userService: UserService, 
		private router: Router, 
		private route:ActivatedRoute,
		private storage:StorageHelper) {
	
	route.data
	    .subscribe(data => {
	    	console.log(data);
	    	this.model = data.data;
	    },
	    error => {
	    	console.log(error['error']);
	    	this.errors = error['error'];
	    });
	}

	onSubmit() { 
		this.success = false;
		this.errors = [];
		this.userService.update(this.model).subscribe(data => {
		  this.success = true;
		},
		error => {
		  console.log(error['error']);
		  this.errors = error['error'];
		});
		console.log(this.model);
	}
	
	ngOnInit() {

	}

	public getUserRole()
	  {
	   var storage = JSON.parse(localStorage.getItem('currentUser'));
	    if (storage && storage.roles)
	      return storage.roles;
	    else
	      return false; 
	  }

}

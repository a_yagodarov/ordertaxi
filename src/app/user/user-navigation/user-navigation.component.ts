import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../_services/user.service';
import { User } from '../../_models/user';
@Component({
  selector: 'app-user-navigation',
  templateUrl: './user-navigation.component.html',
  styleUrls: ['./user-navigation.component.css']
})
export class UserNavigationComponent implements OnInit {
  role : any;
  constructor(private route:ActivatedRoute, private userService: UserService) {
    route.data
      .subscribe(data => {
        this.role = data.data['role'];
      },
      error => {
        console.log(error['error']);
      });
  }

  ngOnInit() {

  }

}

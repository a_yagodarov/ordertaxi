import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contract } from '../_models/contract';

@Injectable()
export class UserContractService {
    constructor(private http: HttpClient) { }

    getAll(params = {}) {
        return this.http.get<Contract[]>('/api/user-contracts', params);
    }

    getById(id: number) {
        return this.http.get<Contract>('/api/user-contract/' + id);
    }

    create(model: any) {
        return this.http.post('/api/user-contract', model);
    }

    update(model: Contract) {
        return this.http.put('/api/user-contract/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/user-contract/' + id);
    }
}

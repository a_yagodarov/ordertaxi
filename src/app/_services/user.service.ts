﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll(params = {}) {
        return this.http.get<User[]>('/api/users', params);
    }

    getLegalsList(params = {})
    {
        return this.http.get<User[]>('/api/legal-list', params);
    }

    getById(id: number) {
        return this.http.get<User>('/api/user/' + id);
    }

    create(user: any) {
        return this.http.post('/api/user', user);
    }

    update(user: User) {
        return this.http.put('/api/user/' + user.id, user);
    }

    delete(id: number) {
        return this.http.delete('/api/user/' + id);
    }
}
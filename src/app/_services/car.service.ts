import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Car } from '../_models/car';
@Injectable()
export class CarService {
    constructor(private http: HttpClient) { }

    getAll(params = {}) {
        return this.http.get<Car[]>('/api/cars', params);
    }

    getById(id: number) {
        return this.http.get<Car>('/api/car/' + id);
    }

    create(car: Car) {
        return this.http.post('/api/car', car);
    }

    update(car: Car) {
        return this.http.put('/api/car/' + car.id, car);
    }

    delete(id: number) {
        return this.http.delete('/api/car/' + id);
    }
}

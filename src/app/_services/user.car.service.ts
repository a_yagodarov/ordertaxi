import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Car } from '../_models/car';

@Injectable()
export class UserCarsService {
    constructor(private http: HttpClient) { }

    getAll(params = {}) {
        return this.http.get<Car[]>('/api/user-cars', params);
    }

    default(id: number) {
        return this.http.get('/api/user-car-default/' + id);
    }

    block(id: number) {
        return this.http.get('/api/user-car-block/' + id);
    }

    create(model: any) {
        return this.http.post('/api/user-car', model);
    }

    // update(model: UserCar) {
    //     return this.http.put('/api/user-car/' + model.id, car);
    // }

    delete(id: number) {
        return this.http.delete('/api/user-car/' + id);
    }
}

import { Injectable } from '@angular/core';

@Injectable()
export class StorageHelper {
   constructor() {

   }
	public getUserName()
	{
		var storage = JSON.parse(localStorage.getItem('currentUser'));
		if (storage && storage.user)
		  return storage.user.name;
		else
		  return false;
	}

	public getUserId()
	{
		var storage = JSON.parse(localStorage.getItem('currentUser'));
		if (storage && storage.user)
		  return storage.user.id;
		else
		  return false;
	}

	public getUserRole()
	{
		var storage = JSON.parse(localStorage.getItem('currentUser'));
		if (storage && storage.roles)
		  return storage.roles;
		else
		  return false; 
	}
	getStartUrl(){
		let role;
		if (role = this.getUserRole())
			return this.getReturnUrl(role);
		else
			return "login";
	}
	getLoginUrl(){
		return "login";
	}
	getReturnUrl(role) {
		switch (role){
		    case "admin": return "orders";
		    case "driver" : return "orders";
		    case "legal" : return "orders";
		    case "driver-manager" : return "orders";
		}
	}
}
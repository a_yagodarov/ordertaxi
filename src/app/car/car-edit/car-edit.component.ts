import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Car } from '../../_models/car';
import { CarService } from "../../_services/car.service";
declare var jQuery:any;
declare var ymaps: any;
declare var $: any;
/* tslint:disable */
@Component({
  selector: 'car-edit',
  templateUrl: './car-edit.component.html',
})
export class CarEditComponent implements OnInit {
	model = new Car;
	errors: any = [];
	constructor(private carService: CarService, private router: Router, private route:ActivatedRoute) {
	route.data
	    .subscribe(data => {
	    	this.model = data.data;
	    },
	    error => {
	    	console.log(error['error']);
	    	this.errors = error['error'];
	    });
	}
	onSubmit() { 
	    this.model.address = jQuery('#basic').val();
	    this.carService.update(this.model).subscribe(data => {
	      this.router.navigate(['cars']);
	    },
	    error => {
	      console.log(error['error']);
	      this.errors = error['error'];
	    });
	    console.log(this.model);
  	}
	initSelect2(that) {  
      $.fn.select2.amd.require([
        'select2/data/array',
        'select2/utils'
        ], function (ArrayData, Utils) {
            function CustomData ($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
              var result =  ymaps.suggest(params.term).then(function (items) {
              var data = [];
              var output = {
                results : [],
                more : false
              };
                for (var index = 0; index < items.length; ++index) {
                    data.push({
                      id: String(items[index]['displayName']),
                      text: items[index]['displayName'],
                    })
                }
                output.results = data;
                callback(output);
              });                
            };

            $("#basic").select2({
                width:"100%",
                closeOnSelect:false,
                dataAdapter: CustomData
            });
            $('#basic').on('select2:select', function (e) {
              console.log(e);
              $('span.select2-search.select2-search--dropdown > input').val($("#basic").val());
            });
        });   
      var option = new Option(this.model.address, this.model.address, false, false);
      $("#basic").append(option).trigger('change');
 	 }
 	 ngOnInit(){
    	this.initSelect2(this);
 	 }

}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
//import {ActivatedRoute} from "../../../../node_modules/@angular/router/src/router_state";
import {Car} from "../../_models/car";
import {CarService} from "../../_services/car.service";
declare var ymaps: any;
@Component({
    selector: 'cars',
    templateUrl: './cars.component.html',
})
export class CarsComponent implements OnInit {
    currentModel: Car;
    models: Car[] = [];
    count: number;
    currentPage: number = 0;
    sort: string = "id";
    desc: boolean = true;
    columns = [
        { name: 'id' , descr: '#' },
        { name: 'mark' , descr: 'Марка' },
        { name: 'model', descr: 'Модель' },
        { name: 'car_number' , descr: 'Номер авто'},
    ];
    constructor(private carService: CarService, private route:ActivatedRoute) {
        

        route.data
            .subscribe(data => {
                console.log(data['data']['count']);
                this.models = data['data']['models'];
                this.count = data['data']['count'];
            });
    }

    ngOnInit() {
       
    }

    deleteModel(id: number) {
        this.carService.delete(id).subscribe(() => { this.loadAllModels() });
    }

    public loadAllModels() {
        this.carService
            .getAll(this.config())
            .subscribe(
                data => {
                    console.log(data);
                    this.models = data['models'];
                    this.count = data['count']
                });
    }

    setPage(page){
        this.currentPage = page - 1;
        console.log(this.config());
        this.loadAllModels();
    }

    setSort(data){
        this.sort = data['key'];
        this.desc = data['desc'];
        this.loadAllModels();
    }

    public config() {
        return {
            params:{
                page:this.currentPage,
                orderBy:this.sort,
                desc:this.desc
            }
        };
    }
}

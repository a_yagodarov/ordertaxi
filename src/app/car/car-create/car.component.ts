import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Car } from "../../_models/car";
import { CarService } from "../../_services/car.service";
declare var ymaps: any;
declare var $: any;
declare var CustomData: any;
/* tslint:disable */
@Component({
  selector: 'car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  model = new Car;
  errors : any = [];

  constructor(private carService: CarService,private router: Router) { }

  submitted = false;

  onSubmit() { 
    this.model.address = $('#basic').val();
    this.carService.create(this.model).subscribe(data => {
      this.router.navigate(['cars']);
    },
    error => {
      console.log(error['error']);
      this.errors = error['error'];
    });
    console.log(this.model);
  }

  ngOnInit() {
      $.fn.select2.amd.require([
        'select2/data/array',
        'select2/utils'
        ], function (ArrayData, Utils) {
          /* tslint:disable */
            function CustomData ($element, options) :any {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }
          /* tslint:enable */
            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
              var result =  ymaps.suggest(params.term).then(function (items) {
              var data = [];
              var output = {
                results : [],
                more : false
              };
                for (var index = 0; index < items.length; ++index) {
                    data.push({
                      id: String(items[index]['displayName']),
                      text: items[index]['displayName'],
                    })
                }
                output.results = data;
                callback(output);
              });                
            };

            $("#basic").select2({
                width:"100%",
                closeOnSelect:false,
                dataAdapter: CustomData
            });
            $('#basic').on('select2:select', function (e) {
              console.log(e);
              $('span.select2-search.select2-search--dropdown > input').val($("#basic").val());
            });
        });
  }
}
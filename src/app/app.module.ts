﻿﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS , HttpHandler} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';
// used to create fake backend

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor , ErrorInterceptor} from './_helpers/index';
import { AlertService, AuthenticationService, UserService, UserProfileService,
    CarService, OrderService, UserCarsService, UserScheduleService, UserContractService

} from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {UsersResolver,} from './_resolvers/user.resolve';
import {UserCarsResolver} from './_resolvers/user.cars';
import {DriversResolver} from './_resolvers/driver';
import {UserNavigationResolver} from './_resolvers/user.navigation';
import { NavigationComponent } from './nav/navigation.component';
import { UserComponent } from './user/user.component';
import { Grid } from './grid/grid';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { UserEditResolver } from './_resolvers/user.edit';
import { UserContractResolver } from './_resolvers/user.contract';
import { CarsComponent } from './car/cars/cars.component';
import {CarEditComponent} from "./car/car-edit/car-edit.component";
import {CarComponent} from "./car/car-create/car.component";
import {CarsResolver} from "./_resolvers/car.resolve";
import {LegalsResolver} from "./_resolvers/legal";
import {CarsListResolver} from "./_resolvers/car.list";
import {CarEditResolver} from "./_resolvers/car.edit";
import { UserNavigationComponent } from './user/user-navigation/user-navigation.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { UserProfileResolver } from './_resolvers/user.profile';
import { UserScheduleResolver } from './_resolvers/user.schedule';
import { OrdersResolver } from './_resolvers/order.resolve';
import { OrderEditResolver } from './_resolvers/order.edit';
import { OrdersComponent } from './order/orders/orders.component';
import { OrderSelectEditResolver } from './_resolvers/order.select.edit';
import { OrderCreateComponent } from './order/order-create/order-create.component';
import { OrderEditComponent } from './order/order-edit/order-edit.component';
import { UserCarsComponent } from './user/user-cars/user-cars.component';
import { UserScheduleComponent } from './user/user-schedule/user-schedule.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { StorageHelper } from './_helpers/storage.helper';
import { SafeHtmlPipe } from './_helpers/safe.html';
import { UserContractsComponent } from './user/user-contracts/user-contracts.component';
import { OrderSelectComponent } from './order/order-select/order-select.component';
import { OrderSelectEditComponent } from './order/order-select-edit/order-select-edit.component';
import { OrderSelectEditAdminComponent } from './order/order-select-edit/components/order-select-edit-admin/order-select-edit-admin.component';
import { OrderSelectEditLegalComponent } from './order/order-select-edit/components/order-select-edit-legal/order-select-edit-legal.component';
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        routing,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CalendarModule.forRoot(),
        ModalModule.forRoot()
    ],
    declarations: [
        SafeHtmlPipe,
        Grid,
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        NavigationComponent,

        UserComponent,
        UserEditComponent,

        CarsComponent,
        CarEditComponent,
        CarComponent,
        UserNavigationComponent,
        UserProfileComponent,
        OrdersComponent,
        OrderCreateComponent,
        OrderEditComponent,
        UserCarsComponent,
        UserScheduleComponent,
        UserContractsComponent,
        OrderSelectComponent,
        OrderSelectEditComponent,
        OrderSelectEditAdminComponent,
        OrderSelectEditLegalComponent,
    ],
    providers: [
        StorageHelper,
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserProfileService,
        UserContractService,
        UserService,
        UsersResolver,
        UserEditResolver,
        UserCarsResolver,
        UserProfileResolver,
        UserNavigationResolver,
        CarService,
        CarsResolver,
        CarEditResolver,
        UserCarsService,
        OrderService,
        OrdersResolver,
        OrderEditResolver,
        OrderSelectEditResolver,
        UserScheduleService,
        UserScheduleResolver,
        CarsListResolver,
        DriversResolver,
        LegalsResolver,
        UserContractResolver,
        // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
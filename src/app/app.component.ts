﻿import { Component, OnInit } from '@angular/core';
import { StorageHelper } from './_helpers/storage.helper';
import { Router} from '@angular/router';
@Component({
    moduleId: module.id.toString(),
    selector: 'app',
    templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit{
	constructor(
		private storageHelper: StorageHelper,
		private router: Router,
		){
		this.router = router;
	} 
	ngOnInit(){
		this.router.events.subscribe((url:any) => {
			if (url.url && url.url == '/')
			{
				this.router.navigate(['/'+this.storageHelper.getStartUrl()]);
			}
		});
		console.log(this.storageHelper.getStartUrl());
		console.log(this.router.url);
	}

}
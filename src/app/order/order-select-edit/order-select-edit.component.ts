import { Component, OnInit } from '@angular/core';
import { Order } from '../../_models/order';
import { Router, ActivatedRoute } from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { UserService } from '../../_services/user.service';
import { OrderService } from '../../_services/order.service';
import { CarService } from '../../_services/car.service';
import { UserContractService } from '../../_services/user.contract.service';
import { StorageHelper } from '../../_helpers/storage.helper';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
declare var ymaps: any;
declare var $: any;
declare var require: any;

@Component({
  selector: 'app-order-select-edit',
  template: `
      <app-order-select-edit-admin *ngIf="storage.getUserRole() == 'admin' || storage.getUserRole() == 'driver-manager'"></app-order-select-edit-admin>
      <app-order-select-edit-legal *ngIf="storage.getUserRole() == 'legal'"></app-order-select-edit-legal>`,
  styleUrls: ['./css/order-select-edit.component.css']
})
export class OrderSelectEditComponent implements OnInit {
  	constructor(
          private router: Router,
          private route: ActivatedRoute,
          private formBuilder: FormBuilder, 
          private userService: UserService, 
          private orderService: OrderService,
          private userProfile: UserContractService,
          private carService: CarService,
          private storage: StorageHelper) {
    }

  	ngOnInit() {

  	}
}

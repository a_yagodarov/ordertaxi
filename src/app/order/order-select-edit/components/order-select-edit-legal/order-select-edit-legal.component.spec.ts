import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSelectEditLegalComponent } from './order-select-edit-legal.component';

describe('OrderSelectEditLegalComponent', () => {
  let component: OrderSelectEditLegalComponent;
  let fixture: ComponentFixture<OrderSelectEditLegalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSelectEditLegalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSelectEditLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

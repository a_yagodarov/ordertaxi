import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSelectEditAdminComponent } from './order-select-edit-admin.component';

describe('OrderSelectEditAdminComponent', () => {
  let component: OrderSelectEditAdminComponent;
  let fixture: ComponentFixture<OrderSelectEditAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSelectEditAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSelectEditAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

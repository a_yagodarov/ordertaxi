import { Component, OnInit } from '@angular/core';
import { Order } from '../../_models/order';
import { Router } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { UserService } from '../../_services/user.service';
import { OrderService } from '../../_services/order.service';
import { CarService } from '../../_services/car.service';
import { UserContractService } from '../../_services/user.contract.service';
import { StorageHelper } from '../../_helpers/storage.helper';
declare var ymaps: any;
declare var $: any;
/* tslint:disable */
@Component({
  selector: 'app-order-create',
  templateUrl: './order-create.component.html',
  styleUrls: ['./order-create.component.css']
})
export class OrderCreateComponent implements OnInit {
	model : FormGroup;
    profileDefault = {
        'transfer' : 0,
        'order_cost' : 0,
        'request_price' : 0
    };
    profile : any = this.profileDefault;
    usersList = [];
    carsList = [];
    driversList = [];
  	errors : any = [];
  	addresses : any = [];
    length = '0';
    statusList = [
        "Новый",
        "В обработке",
        "Принят",
        "На исполнении",
        "Выполнен",
        "Отменен"
    ];
  	constructor(
          private router: Router,
          private formBuilder: FormBuilder, 
          private userService: UserService, 
          private orderService: OrderService,
          private userProfile: UserContractService,
          private carService: CarService,
          private storage: StorageHelper) { }

  	ngOnInit() {
  		this.model = this.formBuilder.group({
		    datetime_order: '',
		    number: '',
            legal_id : '',
            driver_id : '',
		    from: '',
		    comment: '',
            payment: 'noncash',
            price: 0,
            status: 0,
            car_id : '',
		    addresses: this.formBuilder.array([ this.createItem() ])
		  });
        (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
          this.getPrice();
        });
        
        this.model.get('from').valueChanges.subscribe(values => {
          this.getPrice();
        });
        this.model.get('datetime_order').valueChanges.subscribe(values => {
          this.getPrice();
        });
        this.model.get('driver_id').valueChanges.subscribe(values => {
            this.carsList = [];
            if (values)
                this.carService.getAll({params : {all:true, driver_id:values}}).subscribe(data => {
                    var models = data['models'];
                    var defaultModel = null;
                    for (let item of models) {
                        if (item['default'] == 1 && item['blocked'] == 0)
                          defaultModel = item['id'];
                        this.carsList.push({
                          'id' : item['id'],
                          'name' : item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
                        });
                    }
                    this.model.controls["car_id"].setValue(defaultModel);
                });
            else
                this.carsList = [];
        });
        this.initSelect2('#from', '', this.model);
        this.initSelect2('#address_', 0, this.model);
        if (this.storage.getUserRole() == 'admin'){
            this.userService.getLegalsList(this.getUserListParams()).subscribe(data => {
                this.usersList = data['models'];
                this.model.get('legal_id').valueChanges.subscribe(values => {
                    if (values > 0)
                    {
                        this.getProfile(values);
                    }
                    else 
                    {
                        this.profile = this.profileDefault;
                    }
                });
            });
            this.userService.getAll({params : {all:true,blocked:'0',role:'driver'}}).subscribe(data => {
                var users = data['users'];
                for (let item of users) {
                    if (item['id'])
                        this.driversList.push({
                            id: item['id'],
                            name : item['profile_name'] ? (item['profile_name'].split(" ")[0]+" "+item['profile_name'].split(" ")[1]) : ''
                        });
                }
            });
        }
        else {
            var legal_id = this.storage.getUserId();
            this.getProfile(legal_id);
            this.model.controls["legal_id"].setValue(legal_id);
        }
  	}

  	createItem(): FormGroup {
	  return this.formBuilder.group({
	    name: ''
	  });
	}

	addItem(): void {
	  	this.addresses = this.model.get('addresses') as FormArray;
	  	this.addresses.push(this.createItem());
    	this.initSelect2("#address_",(Number(this.addresses.length)-1), this.model);
	}

    removeItem(id) : void {
        if (this.addresses.length > 1)
            this.addresses.removeAt(id);
    }

	onSubmit() { 
		this.orderService.create(this.model.value).subscribe(data => {
            this.router.navigate(['/orders']);
        }, error => {
            this.errors = error['error'];
        });
	}

    initSelect2(selector, id:any = '', model:any)
    {
        $.fn.select2.amd.require([
            'select2/data/array',
            'select2/utils'
        ],  (ArrayData, Utils) => {

            function CustomData ($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
                var result =  ymaps.suggest(params.term).then(function (items) {
                    var data = [];
                    var output = {
                        results : [],
                        more : false
                    };
                    for (var index = 0; index < items.length; ++index) {
                        data.push({
                            id: String(items[index]['displayName']),
                            text: items[index]['displayName'],
                        })
                    }
                    output.results = data;
                    callback(output);
                });
            };

            $(selector+String(id)).select2({
                width:"100%",
                closeOnSelect:false,
                dataAdapter: CustomData,
                minimumInputLength: 2
            });

            $(selector+String(id)).on('select2:select', (e)  => {
                var val = $(selector+String(id)).val();
                $('span.select2-search.select2-search--dropdown > input').val(val);
                if (id !== '')
                {
                    this.model.controls.addresses.value[id].name = val;
                    this.getPrice();
                }
                else
                    this.model.controls["from"].setValue(val);
            });
        });
    }

    getPriceDisabled()
      {
          if (this.storage.getUserRole() == 'admin')
          {
              return false;
          }
          else
              return true;
      }

    getProfile(values)
    {
        this.userProfile.getAll({
          params : {
            user_id : values,
            date_now : true
          }
        }).subscribe(
            data => {
                if (data['models'][0])
                  {
                    this.profile = data['models'][0];
                    this.getPrice();
                  }
            },
            data => {
            });
    }

    getPrice()
    {
      if (this.model.controls['datetime_order'].value && this.model.controls['from'].value
        && this.model.controls['addresses'].value[0].name
        )
        setTimeout(() =>{
        this.orderService.getPrice(this.model.value).subscribe(data => {
            this.model.controls["price"].setValue(data['price']);
            this.length = data['length'] + ' км';
        });
      },100);
    }

    getUserListParams()
    {
        return {
            params: {
                all : true,
                roles : 'legal'
            }
        }
    }
}

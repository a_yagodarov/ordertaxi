import { Component, OnInit } from '@angular/core';
import { Order } from '../../_models/order';
import { Router } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { UserService } from '../../_services/user.service';
import { OrderService } from '../../_services/order.service';
import { CarService } from '../../_services/car.service';
import { UserContractService } from '../../_services/user.contract.service';
import { StorageHelper } from '../../_helpers/storage.helper';
declare var ymaps: any;
declare var $: any;
declare var require: any;
/* tslint:disable */

@Component({
  selector: 'app-order-select',
  // templateUrl: './order-select.component.html',
  template: `
      <div *ngIf="storage.getUserRole() == 'admin' || storage.getUserRole() == 'driver-manager'">${require('./order-select.component.html')}</div>
      <div *ngIf="storage.getUserRole() == 'legal'">${require('./order-select.component-legal.html')}</div>`,
  styleUrls: ['./order-select.component.css']
})
export class OrderSelectComponent implements OnInit {
	model : FormGroup;
    profileDefault = {
        'transfer' : 0,
        'order_cost' : 0,
        'request_price' : 0
    };
    profile : any = this.profileDefault;
    usersList = [];
    carsList = [];
    driversList = [];
  	errors : any = [];
  	addresses : any = [];
    length = '0';
    serve_length = '0';
    statusList = [
        "Новый",
        "В обработке",
        "Принят",
        "На исполнении",
        "Выполнен",
        "Отменен"
    ];
  	constructor(
          private router: Router,
          private formBuilder: FormBuilder, 
          private userService: UserService, 
          private orderService: OrderService,
          private userProfile: UserContractService,
          private carService: CarService,
          private storage: StorageHelper) { }

  	ngOnInit() {
  		this.model = this.formBuilder.group({
  		    datetime_order: '',
  		    number: '',
          legal_id : '',
          driver_id : '',
  		    from: '',
  		    comment: '',
          payment: 'noncash',
          price: 0,
          status: 0,
          car_id : '',
  		    addresses: this.formBuilder.array([ this.createItem() ])
  		  });

        (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
          setTimeout(() => {this.getPrice()}, 50);
        });
        
        this.model.get('from').valueChanges.subscribe(values => {
          setTimeout(() => {
            this.getCars(this.model.get('datetime_order').value);
            this.getPrice()
          }, 50);
        });

        this.model.get('car_id').valueChanges.subscribe(values => {
          if (values)
          {
            setTimeout(() => {this.getProfile(values);
            this.getPrice();}, 50);
          }
        });

        this.model.get('datetime_order').valueChanges.subscribe(values => {
          if (values)
          {
            setTimeout(() => {
              this.getPrice();
              this.getCars(values);
            }, 50);
          }
        });
        this.initSelect2('#from', '', this.model);
        this.initSelect2('#address_', 0, this.model);
        var legal_id = this.storage.getUserId();
        this.model.controls["legal_id"].setValue(legal_id);
  	}

  	getCars(date)
  	{
      if (this.model.get('from').value != '' && this.model.get('datetime_order').value != '')
      {
        this.carsList = [];
        this.carService.getAll({params : {all:true, datetime_order:date, from:this.model.get('from').value}}).subscribe(data => {
              var models = data['models'];
              var defaultModel = null;
              this.carsList = [];
              for (let item of models) {
                  if (item['default'] == 1 && item['blocked'] == 0)
                    defaultModel = item['id'];
                  this.carsList.push({
                    'id' : item['id'],
                    'name' : item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
                  });
              }
              this.model.controls["car_id"].setValue(defaultModel);
          });
      }
    	}

  	createItem(): FormGroup {
	  return this.formBuilder.group({
	    name: ''
	  });
	}

	addItem(): void {
	  	this.addresses = this.model.get('addresses') as FormArray;
	  	this.addresses.push(this.createItem());
    	this.initSelect2("#address_",(Number(this.addresses.length)-1), this.model);
	}

    removeItem(id) : void {
        if (this.addresses.length > 1)
            this.addresses.removeAt(id);
    }

	onSubmit() { 
		this.orderService.createSelect(this.model.value).subscribe(data => {
            this.router.navigate(['/orders']);
        }, error => {
            this.errors = error['error'];
        });
	}


  getPriceDisabled()
    {
        if (this.storage.getUserRole() == 'admin')
        {
            return false;
        }
        else
            return true;
    }

    getProfile(values)
    {
      this.carService.getById(values).subscribe(
          data => {
              if (data)
                {
                  this.profile = data;
                }
          },
          data => {
          });
    }

    getOrderCarDisabled()
    {
        if (!(this.model.controls['datetime_order'].value 
          && this.model.controls['from'].value))
        {
          return true
        }
        else
        {
          return false;
        }
    }

    getPrice()
    {
      if (this.model.controls['datetime_order'].value && this.model.controls['from'].value
        && this.model.controls['addresses'].value[0].name && this.model.controls['car_id'].value
        )
        setTimeout(() =>{
        this.orderService.getDriverPrice(this.model.value).subscribe(data => {
            this.model.controls["price"].setValue(Math.ceil(data['price']));
            this.length = String(Math.ceil(data['length'])) + ' км';
            this.serve_length = String(Math.ceil(data['serve_length'])) + ' км';
        });
      },100);
    }

    getUserListParams()
    {
        return {
            params: {
                all : true,
                roles : 'legal'
            }
        }
    }


    initSelect2(selector, id:any = '', model:any)
    {
        $.fn.select2.amd.require([
            'select2/data/array',
            'select2/utils'
        ],  (ArrayData, Utils) => {

            function CustomData ($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
                var result =  ymaps.suggest(params.term).then(function (items) {
                    var data = [];
                    var output = {
                        results : [],
                        more : false
                    };
                    for (var index = 0; index < items.length; ++index) {
                        data.push({
                            id: String(items[index]['displayName']),
                            text: items[index]['displayName'],
                        })
                    }
                    output.results = data;
                    callback(output);
                });
            };

            $(selector+String(id)).select2({
                width:"100%",
                closeOnSelect:false,
                dataAdapter: CustomData,
                minimumInputLength: 2
            });

            $(selector+String(id)).on('select2:select', (e)  => {
                var val = $(selector+String(id)).val();
                $('span.select2-search.select2-search--dropdown > input').val(val);
                if (id !== '')
                {
                    this.model.controls.addresses.value[id].name = val;
                    this.getPrice();
                }
                else
                    this.model.controls["from"].setValue(val);
            });
        });
    }
}

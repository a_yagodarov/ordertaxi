import { Component, OnInit } from '@angular/core';
import { Order } from '../../_models/order';
import { Router, ActivatedRoute } from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { UserService } from '../../_services/user.service';
import { OrderService } from '../../_services/order.service';
import { CarService } from '../../_services/car.service';
import { UserContractService } from '../../_services/user.contract.service';
import { StorageHelper } from '../../_helpers/storage.helper';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
declare var ymaps: any;
declare var $: any;

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.css']
})
export class OrderEditComponent implements OnInit {

	model : FormGroup;
	id: any;
     profileDefault = {
        'transfer' : 0,
        'order_cost' : 0,
        'request_price' : 0
    };
    profile : any = this.profileDefault;
    usersList = [];
    carsList = [];
    driversList = [];
  	errors : any = [];
  	addresses : any = [];
    length = "";
    statusList = [
        "Новый",
        "В обработке",
        "Принят",
        "На исполнении",
        "Выполнен",
        "Отменен"
    ];
  	constructor(
          private router: Router,
          private route: ActivatedRoute,
          private formBuilder: FormBuilder, 
          private userService: UserService, 
          private orderService: OrderService,
          private userProfile: UserContractService,
          private carService: CarService,
          private storage: StorageHelper) {
    }

  	ngOnInit() {
      this.model = this.formBuilder.group({
          id : '',
          datetime_order: '',
          number: '',
          legal_id : '',
          driver_id : '',
          from: '',
          comment: '',
          payment: 'noncash',
          price: 0,
          status,
          car_id : '',
      addresses: this.formBuilder.array([])
      });
        
      if(this.storage.getUserRole() == 'legal')
      {
          this.resolveRoute(this.route);
      }
      if (this.storage.getUserRole() == 'admin' || this.storage.getUserRole() == 'driver-manager'){
          let source = Observable.forkJoin(
              this.userService.getLegalsList(this.getUserListParams()),
              this.userService.getAll({params : {all:true,blocked:'0',role:'driver'}}),
              // this.route.data
              ).subscribe(([legals, drivers]) => {
                      this.resolveLegals(legals);
                      this.resolveDrivers(drivers);
                      this.resolveRoute(this.route);
                      (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
                          this.getPrice();
                      });

                      this.model.get('from').valueChanges.subscribe(values => {
                          this.getPrice();
                      });

                      this.model.get('legal_id').valueChanges.subscribe(values => {
                          if (values)
                              this.getPrice()
                          else
                              this.profile = this.profileDefault;
                      });
                  }
              );
      }
      else {
          var legal_id = this.storage.getUserId();
          this.getProfile(legal_id);
          this.model.controls["legal_id"].setValue(legal_id);

      }

  		this.id = this.route.snapshot.params['id'];

      this.model.get('driver_id').valueChanges.subscribe(values => {
          this.carsList = [];
          if (values)
              this.carService.getAll({params : {all:true, driver_id:values}}).subscribe(data => {
                  var models = data['models'];
                  for (let item of models) {
                      this.carsList.push({
                        'id' : item['id'],
                        'name' : item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
                      });
                  }
              });
          else
              this.carsList = [];
      });
  	}

    getPriceDisabled()
    {
        if (this.storage.getUserRole() == 'admin')
        {
            return false;
        }
        else
            return true;
    }

    resolveLegals(legals){
        this.usersList = legals['models'];
        this.model.get('legal_id').valueChanges.subscribe(values => {
            if (values > 0)
            {
                this.getProfile(values);
            }
            else 
            {
                this.profile = this.profileDefault;
            }
        });
    }

    resolveDrivers(drivers){
        var users = drivers['users'];
        for (let item of users) {
            if (item['id'])
                this.driversList.push({
                    id: item['id'],
                    name :item['profile_name'].split(" ")[0]+" "+item['profile_name'].split(" ")[1]
                });
        }
    }

  	resolveRoute(route) : void {
  		route.data
	        .subscribe(data => { 
				let httpModel = data['data'];
	  			Object.keys(httpModel).forEach(k => {
					let control = this.model.get(k);
					if (control instanceof FormControl)
						control.setValue(httpModel[k], {onlySelf:true});
					if (control instanceof FormArray)
					{
						for (let i in httpModel[k])
						{
							if (i == '0')
							{
								this.addresses = this.model.get('addresses') as FormArray;
								this.addresses.push(this.formBuilder.group({
							    	name: httpModel[k][i]['where']
							  	}));
								this.initSelect2("#address_", i, this.model);
							}
							else
							{
								this.addresses = this.model.get('addresses') as FormArray;
								this.addresses.push(this.formBuilder.group({
							    	name: httpModel[k][i]['where']
							  	}));
							  	this.initSelect2("#address_", i, this.model);
							}	
						}
					}
				});
        var t = httpModel['datetime_order'].split(/[- :]/);
        var date = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
				this.model.get('datetime_order').setValue(date.toISOString().substr(0, 16));
				this.initSelect2('#from', '', this.model);
				var newOption = new Option(httpModel.from, httpModel.from, false, false);
				$('#from').append(newOption).trigger('change');
                (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
                    this.getPrice();
                });

                this.model.get('from').valueChanges.subscribe(values => {
                    this.getPrice();
                });
        });
        this.getPrice();
  	}

    canAction()
    {
        if (this.storage.getUserRole() == 'admin' || this.storage.getUserRole() == 'driver-manager')
        {
            return true;
        }
        else
        {
            if (this.model.controls['status'].value == '5' || this.model.controls['status'].value == '4')
                return false;
            else
                return true;
        }
    }

	createItem(name = ''): FormGroup {
    return this.formBuilder.group({
      name: name
    });
	}

	addItem(name = ''): void {
  	this.addresses = this.model.get('addresses') as FormArray;
  	this.addresses.push(this.createItem(name));
  	this.initSelect2("#address_",(Number(this.addresses.length)-1), this.model);
	}

    removeItem(id) : void {
        if (this.addresses.length > 1)
            this.addresses.removeAt(id);
    }

    cancelOrder() : void {
      if (confirm("Отменить заказ?"))
      {
        this.orderService.cancelOrder(this.model.controls['id'].value).subscribe(data => {
            this.router.navigate(['/orders']);
        }, error => {
            console.log(error);
        });
      }
    }

  	onSubmit() { 
  		this.orderService.update(this.model.value).subscribe(data => {
          this.router.navigate(['/orders']);
      }, error => {
          this.errors = error['error'];
      });
  	}

    initSelect2(selector, id:any = '', model:any)
    {
        $.fn.select2.amd.require([
            'select2/data/array',
            'select2/utils'
        ],  (ArrayData, Utils) => {

            function CustomData ($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
                var result =  ymaps.suggest(params.term).then(function (items) {
                    var data = [];
                    var output = {
                        results : [],
                        more : false
                    };
                    for (var index = 0; index < items.length; ++index) {
                        data.push({
                            id: String(items[index]['displayName']),
                            text: items[index]['displayName'],
                        })
                    }
                    output.results = data;
                    callback(output);
                });
            };

            $(selector+String(id)).select2({
                width:"100%",
                closeOnSelect:false,
                dataAdapter: CustomData,
                minimumInputLength: 2
            });

            $(selector+String(id)).on('select2:select', (e)  => {
                var val = $(selector+String(id)).val();
                $('span.select2-search.select2-search--dropdown > input').val(val);
                if (id !== '')
                {
                    // this.model.controls["addresses"]["value"][id]["name"] = val;
                    this.model.controls.addresses.value[id].name = val;
                    this.getPrice();
                	// model.value['addresses'][id]['name'] = val;
                }
                else
                    this.model.controls["from"].setValue(val);
                	// model.value['from'] = val;
                // this.callback(123);
            });

            if (id !== '')
            {
				var val = this.addresses.value[id]['name'];
				var newOption = new Option(val, val, false, false);
				$(selector+id).append(newOption).trigger('change');
            }
            else
            {
            	var val = this.model.controls["from"].value;
				var newOption = new Option(val, val, false, false);
				$(selector).append(newOption).trigger('change');	
            }
        });
    }

    getProfile(values)
    {
        this.userProfile.getAll({
          params : {
            user_id : values,
            date_now : true
          }
        }).subscribe(
            data => {
                if (data['models'][0])
                  {
                    this.profile = data['models'][0];
                  }
                // this.getPrice();
            },
            data => {
            });

    }

    getPrice()
    {
       setTimeout(() =>{
        this.orderService.getPrice(this.model.value).subscribe(data => {
            this.model.controls["price"].setValue(Math.ceil(data['price']));
            this.length = data['length'] + ' км';
        });
      },100);
        
    }

    getUserListParams()
    {
        return {
            params: {
                all : true,
                roles : 'legal'
            }
        }
    }
}

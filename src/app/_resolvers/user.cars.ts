/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Car} from "../_models/car";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserCarsService} from "../_services/user.car.service";

@Injectable()
export class UserCarsResolver implements Resolve<Car[]>{
  constructor(private service: UserCarsService) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Car[]> {
    return this.service.getAll({
    	params : {
    		id : Number(route['_urlSegment']['segments'][1]['path'])
    	}
  });
  }

}
export class Car {
    id: number;
    mark: string;
    model: string;
    car_number: string;
    color: string;
    year: number;
    passengers: number;
    baggage: number;
    baby_chair: any;
    conditioner: any;
    bank_card_payment: any;
    comment: string;
    address:any;
    request_price:any;
    transfer:any;
    order_cost:any;
}

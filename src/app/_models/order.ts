export class Order {
    id: number;
    datetime_order?: any;
    number?: any;
    from?: any;
    comment?: any;
    payment?: any;
    price?: any;
    driver_id?: any;
    legal_id?: any;
    car_id?: any;
    status?: any;
    addresses?: any[];
}

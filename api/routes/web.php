<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('authenticate', 'Auth\LoginController@authenticate');
Route::post('auth-phone', 'Auth\LoginController@authenticatePhone');
Route::post('auth-sms', 'Auth\LoginController@authenticateSms');

Route::group(['middleware' => ['jwt.auth', 'role:driver|admin|legal']], function () {
	Route::post('order-status', 'OrderController@status');
	
	
	Route::get('user/{id}', 'UserController@get');
	Route::put('user/{id}', 'UserController@update');

	Route::get('profile/{id}', 'ProfileController@get');
	Route::put('profile/{id}', 'ProfileController@update');

	Route::get('user-schedules', 'ScheduleController@get');
	Route::delete('user-schedule/{id}', 'ScheduleController@delete');
	Route::post('user-schedule/', 'ScheduleController@store');
	Route::put('user-schedule/{id}', 'ScheduleController@update');

	
});
Route::group(['middleware' => ['jwt.auth', 'role:driver|admin']], function () {
	
	Route::put('order-decline/', 'OrderController@declineDriver');
	Route::get('user-cars/', 'UserCarsController@get');
	
});
Route::group(['middleware' => ['jwt.auth', 'role:admin|legal']], function () {
	Route::post('order-price', 'OrderController@getPrice');;
	Route::post('order-driver-price', 'OrderController@getDriverPrice');;
	Route::post('order', 'OrderController@store');
	Route::post('order-select', 'OrderController@storeSelect');
	
	Route::get('order-cancel/{id}', 'OrderController@cancelOrder');
	Route::get('order/{id}', 'OrderController@get');
	
});

Route::group(['middleware' => ['jwt.auth', 'role:admin|legal|driver-manager']], function () {
	Route::get('user-contracts/', 'UserContractController@index');
	Route::get('car/{id}', 'CarController@get');
	Route::put('order/{id}', 'OrderController@update');
	Route::put('order-select/{id}', 'OrderController@updateSelect');
});

Route::group(['middleware' => ['jwt.auth', 'role:driver|admin|legal|driver-manager']], function () {
	Route::get('orders/', 'OrderController@index');
	Route::get('order/{id}', 'OrderController@get');
	Route::get('order-select/{id}', 'OrderController@getSelect');
	Route::get('users/', 'UserController@index');
	Route::get('cars/', 'CarController@index');
	Route::get('orders/', 'OrderController@index');
});

Route::group(['middleware' => ['jwt.auth', 'role:admin|driver-manager']], function () {
	Route::get('legal-list/', 'UserController@legalList');
});

Route::group(['middleware' => ['jwt.auth', 'role:admin']], function () {
	
	Route::get('user-contract/{id}', 'UserContractController@get');
	Route::post('user-contract/', 'UserContractController@store');
	Route::put('user-contract/{id}', 'UserContractController@update');
	Route::delete('user-contract/{id}', 'UserContractController@delete');

	Route::delete('user-car/{id}', 'UserCarsController@delete');
	Route::get('user-car-block/{id}', 'UserCarsController@block');
	Route::get('user-car-default/{id}', 'UserCarsController@default');
	Route::post('user-car/', 'UserCarsController@store');	
	
	Route::delete('user/{id}', 'UserController@delete');
	Route::post('user', 'UserController@store');

	Route::delete('car/{id}', 'CarController@delete');
	Route::post('car', 'CarController@store');
	Route::put('car/{id}', 'CarController@update');

	Route::delete('order/{id}', 'OrderController@delete');

	Route::delete('profile/{id}', 'ProfileController@delete');
	Route::post('profile', 'ProfileController@store');


});
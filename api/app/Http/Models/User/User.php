<?php

namespace App\Http\Models\User;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
	use HasRoles;
    use Notifiable;
	protected $guard_name = 'web';
	public $pw;
	public static function search(Request $request)
	{
		$users =  DB::table('users')
			->select('users.id', 'users.name', 'users.email', 'roles.name as role', 'profile.name as profile_name')
			->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
			->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
			->leftJoin('profile', 'profile.id', '=', 'users.id')
			->when($request->get('name'), function($users) use ($request){
				return $users->where('users.name', 'LIKE', "%{$request->get('name')}%");
			})
			->when($request->get('email'), function($users) use ($request){
				return $users->where('users.email', 'LIKE', "%{$request->get('email')}%");
			})
			->when($request->get('orderBy'), function ($users) use ($request) {
				return $users
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($users) use ($request) {
				return $users
					->orderBy('users.id', 'desc');
			})
			->when($request->get('blocked') > -1, function ($users) use ($request){
				return $users->where('users.blocked', '=', $request->get('blocked'));
			})
			->when($request->get('role'), function ($users) use ($request){
				return $users->where('roles.name', '=', $request->get('role'));
			})
			->groupBy('id');

		$count = $users->get()->count();
		$users = $users

			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request){
				return $users->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'users' => $users,
			'count' => $count
		]);
	}

	public static function getLegalsList(Request $request)
	{
		$users =  DB::table('users')
		->select('users.id as id', 'profile.name as name')
		->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
		->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
		->leftJoin('profile', 'users.id', '=', 'profile.id')
		->where('roles.name', '=', 'legal')
		->get();
		return response()->json([
			'models' => $users,
		]);
	}

	public function rules(Request $request)
	{
		if ($request->isMethod('post')) {
			return [
				'name' => 'required|min:4|max:191|unique:users,name',
				'password' => 'nullable|min:4|max:191',
				'email' => 'required|email|unique:users,email',
				'role' => 'required'
			];
		}
		elseif ($request->isMethod('put')) {
			return [
				'name' => 'required|min:4|max:191|unique:users,name,'.$request->get('id'),
				'password' => 'sometimes|min:4|max:191',
				'email' => 'required|email|unique:users,email,'.$request->get('id'),
				'role' => 'required'
			];
		}
		else {
			abort(403);
		}
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($this->password == ''){
                $this->pw = str_random(6);
	            $this->password = bcrypt($this->pw);
            }
            else
            {
                $this->pw = $request->password;
	            $this->password = bcrypt($request->password);
            }
			if ($result = $this->save())
			{
				$profile = Profile::create(['id' => $this->id]);
				$profile->save();
				$this->sendRegistrationEmail($this);
            	$this->assignRole($request->role);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails() && $user = \App\Http\Models\User\User::find($request->get('id')))
        {
        	$this::find($request->get('id'));
	        $this->fill($request->all());
	        $this->pw = $request->password;
	        if (!$request->password)
	        {
		        $this->password = $this->getOriginal('password');
	        }
	        else
	        {
		        $this->password = bcrypt($request->password);
	        }
	        if ($this->pw || ($this->getOriginal('name') != $this->name)) $this->sendUpdateUserEmail($this);
            if ($result = $this->save())
            {
//	            return response()->json($this->getRoleNames(), 403);
	            $this->removeRole($this->getRoleNames()[0]);
	            $this->assignRole($request->role);
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function cars()
    {
    	return $this->belongsToMany('App\Http\Models\Car', 'user_cars', 'user_id', 'car_id')->withPivot('car_id');
    }

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $fillable = [
        'id', 'name', 'email', 'password', 'blocked'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendUpdateUserEmail($user)
    {
	    if ($this->pw)
	    {
		    $messsage = 'Логин: '.$user->name.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
	    }
	    else
	    {
		    $messsage = 'Логин: '.$user->name.' Для сайта '.$this->getUrl();
	    }
        return Mail::raw($messsage, function ($message) use ($user){
            $message->to($user->email);
            $message->subject('Данные для входа на сайт были обновлены');
        });
    }

    public function sendRegistrationEmail($user)
    {
        $messsage = 'Логин: '.$user->name.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
        return Mail::raw($messsage, function ($message) use ($user){
	            $message->to($user->email);
            $message->subject('Данные для входа на сайт');
        });
    }
    public function getUrl()
    {
    	$url = App::make('url')->to('/');
    	return parse_url($url, PHP_URL_SCHEME).'://'.str_replace('api', '', parse_url($url, PHP_URL_HOST));
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;

use App\Rules;
class Order extends Model
{
	public $length = 0;
	public $serve_length = 0;
    protected $fillable = [
		'datetime_order', 'number', 'from', 'comment', 'payment', 'price',
		'legal_id', 'driver_id', 'car_id', 'status', 'driver_comment', 'scenario'
	];

	public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            // ... code here
        });

        self::created(function($model){
        	$row = (DB::table('profile')->where('id', $model->driver_id)->first());
            if ($row && $row->token && $model->status <= 4)
            {
	            PushNotification::app('appNameAndroid')
	                ->to($row->token)
	                ->send('Order created');
            }
        });

        self::updating(function($model){
            $row = (DB::table('profile')->where('id', $model->driver_id)->first());
            if ($row && $row->token && (in_array($model->getOriginal('status'), [3,4])) &&  $model->status == 5)
            {
	            PushNotification::app('appNameAndroid')
	                ->to($row->token)
	                ->send('Order declined');
            }
        });

        self::updated(function($model){
        	
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }

	public function rules(Request $request)
	{
		return [
			'datetime_order' => ['required', 'date', function($attribute, $value, $fail) use ($request) {
				$user = Auth::user()->hasRole('admin');
				if (!$user)
				{
					$date = Carbon::createFromFormat('Y-m-d\TH:i', $request->get('datetime_order'))->toDateTimeString();
		           	$query = DB::table('user_contract')
					->where([
						['user_id', '=', $request->get('legal_id')],
						['contract_period_start', '<=', "{$date}"],
						['contract_period_end', '>=', "{$date}"],
					])
					->first();
					if (!$query)
						return $fail('Отсутсвует договор на эту дату и время');
				}
					return true;
		        },],
			'number' => 'required',
			'from' => 'required',
			'addresses' => ['required', function($attribute, $value, $fail) {
		            if (is_array($value))
			        {
			            foreach($value as $item)
			            {
			                if ($item['name'] != '')
			                    return true;
			            }

			        }
			        else
			            return $fail("Заполните это поле");
			        return $fail("Заполните это поле");
		        },],
			'comment' => 'sometimes',
			'payment' => 'required|string',
			'price' => 'required|numeric',
			'legal_id' => 'sometimes|numeric|nullable',
			'driver_id' => 'sometimes|numeric|nullable',
			'car_id' => ['sometimes', function($attribute, $value, $fail) use($request){
				if ($request->get('driver_id') && ($request->get('car_id') == ''))
					return $fail("Заполните это поле");
			}],
			'status' => ['required','numeric', function($attribute, $value, $fail) use($request){
				if (Auth::user()->hasRole('legal'))
				{
					$id = $request->get('id');
					$data = DB::table('orders')
						->select('status')
						->where('id', $id)
						->first();
					if ($data && ($data->status == '4' || $data->status == '5'))
						return $fail("Нельзя сменить статус выполненного или отмененного заказа");
				}
			}]
		];
	}

	public function rulesSelect(Request $request)
	{
		return [
			'datetime_order' => ['required', 'date'],
			'number' => 'required',
			'from' => 'required',
			'addresses' => ['required', function($attribute, $value, $fail) {
		            if (is_array($value))
			        {
			            foreach($value as $item)
			            {
			                if ($item['name'] != '')
			                    return true;
			            }

			        }
			        else
			            return $fail("Заполните это поле");
			        return $fail("Заполните это поле");
		        },],
			'comment' => 'sometimes',
			'payment' => 'required|string',
			'price' => 'required|numeric',
			'legal_id' => 'sometimes|numeric|nullable',
			'driver_id' => 'sometimes|numeric|nullable',
			'car_id' => ['sometimes', function($attribute, $value, $fail) use($request){
				if ($request->get('driver_id') && ($request->get('car_id') == ''))
					return $fail("Заполните это поле");
			}],
			'status' => ['required','numeric', function($attribute, $value, $fail) use($request){
				if (Auth::user()->hasRole('legal'))
				{
					$id = $request->get('id');
					$data = DB::table('orders')
						->select('status')
						->where('id', $id)
						->first();
					if ($data && ($data->status == '4' || $data->status == '5'))
						return $fail("Нельзя сменить статус выполненного или отмененного заказа");
				}
			}]
		];
	}

	public static function search(Request $request)
	{
		$rows =  DB::table('orders')
			->select(
				'orders.*', 
				DB::raw('(SELECT GROUP_CONCAT(`where` SEPARATOR " - ") as `to` FROM `order_address` WHERE `order_id` = orders.id) as `to`'), 
				'profile.name as legal_name', 
				DB::raw('CONCAT_WS(" ", cars.mark, cars.model, cars.color, cars.car_number) as car_name'),
				DB::raw("
					CONCAT_WS(
						' ', 
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 1), ' ', -1), 
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 2), ' ', -1)
					) as driver_name

					"),
				'dp.phone as driver_phone'
		)
			->leftJoin('profile', 'profile.id', '=', 'orders.legal_id')
			->leftJoin('profile as dp', 'dp.id', '=', 'orders.driver_id')
			->leftJoin('cars', 'cars.id', '=', 'orders.car_id')
			->when(Auth::user()->hasRole('legal'), function($rows) use ($request) {
				$user = Auth::user();
				return $rows
					->where('legal_id', '=', $user->id);
			})
			->when(Auth::user()->hasRole('driver'), function($rows) use ($request) {
				$user = Auth::user();
				return $rows
					->where('driver_id', '=', $user->id);
			})
			->when($request->get('id'), function($rows) use ($request){
					return $rows->where('orders.id', '=', $request->id);
				})
			->when($request->get('datetime_order'), function($rows) use ($request){
					return $rows->where('orders.datetime_order', 'LIKE', "{$request->datetime_order}%");
				})
			->when($request->get('from'), function($rows) use ($request){
					return $rows->where('orders.from', 'LIKE', "%{$request->from}%");
				})
			->when($request->get('to'), function($rows) use ($request){
					return $rows->whereIn('orders.id', function($rows) use ($request){
					        $rows->select('order_id')
						        ->from('order_address')
						        ->where('order_address.where', 'LIKE', "{$request->to}%");
				        });
				})
			->when($request->get('price'), function($rows) use ($request){
					return $rows->where('orders.price', '>=', $request->price);
				})
			->when($request->get('price2'), function($rows) use ($request){
					return $rows->where('orders.price', '<=', $request->price2);
				})
			->when($request->get('car_id'), function($rows) use ($request){
					return $rows->where('orders.car_id', '=', $request->car_id);
				})
			->when($request->get('driver_id'), function($rows) use ($request){
					return $rows->where('orders.driver_id', '=', $request->driver_id);
				})
			->when($request->get('legal_id'), function($rows) use ($request){
					return $rows->where('orders.legal_id', '=', $request->legal_id);
				})
			->when($request->get('status') > -1, function($rows) use ($request){
					return $rows->where('orders.status', '=', $request->status);
				})
			->when($request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy('orders.id', 'desc');
			})
			->groupBy('id');
		// return response()->json($rows->toSql(), 403);
		$count = $rows->get()->count();
		$rows = $rows
			->when($request->get('page') >= 0, function ($rows) use ($request){
				return $rows->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $rows,
			'count' => $count
		]);
	}

	public function changeStatus(Request $request)
	{
		$user = Auth::user();
		$validator = Validator::make($request->only('id', 'status', 'driver_comment'), [
			'id' => ['required', function($attribute, $value, $fail) use($user){
				if (!($user->id == $this->driver_id))
				{
					return $fail("Заказ не принадлежит водителю");
				}
				return true;
			}],
			'driver_comment' => 'sometimes',
			'status' => ['required', function($attribute, $value, $fail){
				if ($this->status == 2)
				{
					if (!($value == 3 || $value == 5))
					{
						return $fail("Невозможно изменить статус");
					}
				}
				elseif ($this->status == 3)
				{
					if (!($value == 4 || $value == 5))
					{
						return $fail("Невозможно изменить статус");
					}
				}
				elseif ($this->status == 5)
				{
					if (!($value == 5))
						return $fail("Невозможно изменить статус");	
				}
				else {
					return $fail("Невозможно изменить статус");
				}
			}],
		], $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->status = $request->get('status');
			$this->driver_comment = $request->get('driver_comment');
			return response()->json($this->save());
		}
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			// return response()->json($this->getPrice($request), 403);
			if (!Auth::user()->hasRole('admin')) {
				$this->status = 0;
				$this->price = $this->getPrice($request);
			}
			$this->scenario = 'legal';
			if ($result = $this->save())
			{
				$this->saveAdresses($request);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeSelect(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rulesSelect($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			$this->driver_id = $this->getDriverByCarId($this->car_id);
			if (!Auth::user()->hasRole('admin')) {
				$this->status = 0;
				$this->price = $this->getDriverPrice($request);
			}
			$this->scenario = 'driver';
			if ($result = $this->save())
			{
				$this->saveAdresses($request);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeSelectUpdate(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rulesSelect($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			$this->driver_id = $this->getDriverByCarId($this->car_id);
			if (!Auth::user()->hasRole('admin')) {
				$this->price = $this->getDriverPrice($request);
			}
			$this->scenario = 'driver';
			if ($result = $this->save())
			{
				$this->saveAdresses($request);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function getDriverByCarId($id)
	{
		$row = DB::table('cars')->select('users.*')
				->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
				->leftJoin('users', 'user_cars.user_id', '=', 'users.id')
				->where('cars.id', '=', $id)
				->first();
		return ($row->id);
	}

	public function storeUpdate(Request $request)
	{
		$validate = Validator::make($request->all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this->fill($request->all());
			if (!Auth::user()->hasRole('admin')) {
				$this->price = $this->getPrice($request);
			}
			$result = $this->save();
			if ($result)
			{
				$this->saveAdresses($request);
				return response()->json($result, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}

	public function saveAdresses($request)
	{
		DB::table('order_address')->where('order_id', '=', $this->id)->delete();
		foreach($request->get('addresses') as $item)
		{
			DB::table('order_address')->insert(
			    ['order_id' => $this->id, 'where' => $item['name']]
			);
		}
	}

	public function getCoords($arrayOfAddresses)
	{
		$carToStartCoords = [];
		foreach ($arrayOfAddresses as $key => $value) {
			$url = "https://geocode-maps.yandex.ru/1.x/?geocode=".$value."&format=json";  
			$ch = curl_init();  

			// set URL and other appropriate options  
			curl_setopt($ch, CURLOPT_URL, $url);  
			curl_setopt($ch, CURLOPT_HEADER, 0);  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
			$output = json_decode(curl_exec($ch));
			$points = explode(" ", $output->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
			$carToStartCoords[] = [
				'x' => $points[0],
				'y' => $points[1],
			];
			curl_close($ch);
		}
		return $carToStartCoords;
	}

	public function getLengthForTwo($from, $to, $date)
	{
		$time = strtotime(Carbon::createFromFormat('Y-m-d\TH:i',$date)->toDateTimeString());
		return $this->getLength($this->getCoords([$from, $to]), $time);
	}

	public function getLength($coords, $time)
	{
		$apiKey = \Config::get("yandex-map")["apiKey"];
		$length = 0;
		if ($time < time())
		{
			$time = null;
		}
		foreach($coords as $key => $value)
		{
			if ($key > 0 && $coords[$key-1])
			{
				if ($time)
				{
					$url = 'https://api.routing.yandex.net/v1.0.0/distancematrix?origins='.
					$coords[$key-1]['y'].','.$coords[$key-1]['x'].
					"&destinations=".
					$coords[$key]['y'].','.$coords[$key]['x'].
					"&apikey=".$apiKey."&departure_time=".$time."&mode=transit";
				}
				else
				{
					$url = 'https://api.routing.yandex.net/v1.0.0/distancematrix?origins='.
					$coords[$key-1]['y'].','.$coords[$key-1]['x'].
					"&destinations=".
					$coords[$key]['y'].','.$coords[$key]['x'].
					"&apikey=".$apiKey."&mode=transit";	
				}
				$ch = curl_init(); 
				// set URL and other appropriate options  
				curl_setopt($ch, CURLOPT_URL, $url);  
				curl_setopt($ch, CURLOPT_HEADER, 0);  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
				$output = json_decode(curl_exec($ch));
				if(isset($output->errors) || !isset($output->rows))
				{
					
					$url = 'https://api.routing.yandex.net/v1.0.0/distancematrix?origins='.
					$coords[$key-1]['y'].','.$coords[$key-1]['x'].
					"&destinations=".
					$coords[$key]['y'].','.$coords[$key]['x'].
					"&apikey=".$apiKey."&mode=transit";		
					curl_setopt($ch, CURLOPT_URL, $url);
					$output = json_decode(curl_exec($ch));
				}
				if(!isset($output->rows))
				{
					$length = 0;break;
					// abort(403, $url);
				}
				$length += $output->rows[0]->elements[0]->distance->value;
				curl_close($ch);
			}
		}
		return $length;
	}

	public function getPrice($request)
	{
		$time = strtotime(Carbon::createFromFormat('Y-m-d\TH:i', $request->get('datetime_order'))->toDateTimeString());

		$wordAddresses[] = $request->get('from');
		foreach($request->get('addresses') as $item)
		{
			if ($item['name'])
				$wordAddresses[] = $item['name'];
		}
		
		// return $time." ".time();
		$length = $this->getLength($this->getCoords($wordAddresses), $time);
		if ($request->legal_id)
		{
			$date = Carbon::createFromFormat('Y-m-d\TH:i', $request->get('datetime_order'))->toDateTimeString();
			if ($query = DB::table('user_contract')
				->where([
						['user_id', '=', $request->get('legal_id')],
						['contract_period_start', '<=', "{$date}"],
						['contract_period_end', '>=', "{$date}"],
					])
				->first())
				{
					$price = intval(($length * $query->transfer/1000) + $query->request_price);
					if ($price < $query->order_cost) $price = $query->order_cost;
				}
			else
			{
				$price = -1;
			}
		}
		else
			$price = 0;
		$this->length = intval($length/1000);
		return $price;
	}

	public function getDriverPrice($request)
	{
		$time = strtotime(Carbon::createFromFormat('Y-m-d\TH:i', $request->get('datetime_order'))->toDateTimeString());

		$car = DB::table('cars')->where('id', $request->get('car_id'))->first();
		if (!$car)
		{
			$this->length = 0;
			return -1;
		}
		elseif (!$car->address)
		{
			$this->length = 0;
			return -1;	
		}
		$carAddresses[] = $car->address;
		$carAddresses[] = $request->get('from');
		if (count($carAddresses) > 1)
			$carToStartLength = $this->getLength($this->getCoords($carAddresses), $time);
		else
			return -2;
		$carToStartPrice = intval(($carToStartLength * $car->request_price)/1000);

		$wordAddresses[] = $request->get('from');
		foreach($request->get('addresses') as $item)
		{
			if ($item['name'])
				$wordAddresses[] = $item['name'];
		}

		$length = $this->getLength($this->getCoords($wordAddresses), $time);

		if ($request->legal_id)
		{
			$date = Carbon::createFromFormat('Y-m-d\TH:i', $request->get('datetime_order'))->toDateTimeString();
			if ($car->transfer && $car->request_price && $car->order_cost)
			{
				$price = intval(($length * $car->transfer)/1000);
				$price += $carToStartPrice;
				if ($price < intval($car->order_cost)) $price = $car->order_cost;
			}
			else
			{
				$price = -3;
			}
		}
		else
			$price = -4;
		$this->length = intval($length/1000);
		$this->serve_length = intval($carToStartLength/1000);
		return intval($price);
	}

	public function decline(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'driver_comment' => 'sometimes',
			'callsign' => ['sometimes', function($attribute, $value, $fail){
				$profile = DB::table('profile')
					->where([
						['callsign', '=', $value]
					])
					->get();
				if (!$profile)
					return $fail("Не найден водитель с таким позывным");
				else
					return true;
			}],
		], $this->messages());
		if ($validator->fails()) {
			response()->json($validator->errors(), 403);
		}
		else
		{
			$profile = null;
			if ($callsign = $request->get('callsign'))
			{
				$profile = DB::table('profile')
					->where([
						['callsign', '=', $callsign]
					])
					->first();
				$profile = $profile->id;
			}
			$this->driver_comment = $request->get('driver_comment');
			$this->driver_id = $profile;
			$this->car_id = null;
			if (!$result = $this->save())
			{
				return response()->json($result, 403);
			}
			else
			{
				return response()->json($result, 200);	
			}

		}
		return response()->json("false", 403);
	}

	public function setCancelled()
	{
		$user = Auth::user();
		if ($user->hasRole('legal'))
		{
			if ($this->status != 4 && $this->status != 5)
			{
				$this->status = 5;
				$this->car_id = null;
				return response()->json($this->save(), 200);
			}
			else
			{
				return response()->json(['message' => 'Нельзя отменить'], 403);	
			}
		}
		else if ($user->hasRole('admin'))
		{
				$this->status = 5;
				$this->car_id = null;
				return response()->json($this->save(), 200);
		}
		else
			return response()->json(['message' => 'Нельзя отменить'], 403);	
	}

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
			'date' => 'Выберите дату',
			'numeric' => 'Заполните это поле'
		];
	}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
}

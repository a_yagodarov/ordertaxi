<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Http\Models\Order;
class Car extends Model
{
	protected $fillable = [
		'id', 'mark', 'model', 'car_number', 'color', 'year', 'passengers',
		'baggage', 'baby_chair', 'conditioner', 'bank_card_payment',
		'comment', 'address', 'request_price', 'transfer', 'order_cost'
	];

	public function rules(Request $request)
	{
		return [
			'mark' => 'required',
			'model' => 'required',
			'car_number' => 'required',
			'color' => 'sometimes',
			'year' => 'sometimes',
			'passengers' => 'required|numeric',
			'baggage' => 'required|numeric',
			'baby_chair' => 'required|numeric',
			'conditioner' => 'required|numeric',
			'bank_card_payment' => 'required|numeric',
			'comment' => 'sometimes',
			'request_price' => 'required|numeric',
			'transfer' => 'required|numeric',
			'order_cost' => 'required|numeric',
			'address' => 'required',
		];
	}

	public static function search(Request $request)
	{
		$rows =  DB::table('cars')
			->when($request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when($request->get('driver_id'), function($rows) use ($request) {
				$rows->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
				->where('user_id', '=', $request->get('driver_id'))
					->where('blocked', '=', '0');;
				$rows->select('cars.*', 'user_cars.blocked', 'user_cars.default');
			})
			->when($request->get('datetime_order'), function($rows) use ($request) {
				$datetime = Carbon::createFromFormat('Y-m-d\TH:i', $request->get('datetime_order'));
				$cars = DB::table('cars')
					->select("cars.*")
					->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
					->leftJoin('schedule as sc', 'sc.user_id', '=', 'user_cars.user_id')
					->where('user_cars.blocked', '=', '0')
					->whereRaw("sc.date_start <= '{$datetime}' AND '{$datetime}' <= sc.date_end")
					->get();
				if ($request->get('from'))
				{
					$carIds = [];
					foreach($cars as $key => $value)
					{
						$length = (new Order())->getLengthForTwo($request->get('from'), $value->address, $request->get('datetime_order'));
						if ($length < 10000)
						{
							$carIds[] = $value->id;
						}
					}
					$rows->select("cars.*")
					->whereIn("cars.id", $carIds)
					->get();
				}
				else
				{
					$rows->select("cars.*")
					->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
					->leftJoin('schedule as sc', 'sc.user_id', '=', 'user_cars.user_id')
					->where('user_cars.blocked', '=', '0')
					->whereRaw("sc.date_start <= '{$datetime}' AND '{$datetime}' <= sc.date_end")
					->get();
				}
			})
			->when(!$request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy('cars.id', 'desc');
			})
			->groupBy('id');

		$count = $rows->get()->count();
		$rows = $rows
			->when($request->get('page') >= 0 && !$request->get('all'), function ($rows) use ($request){
				return $rows->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $rows,
			'count' => $count
		]);
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
	{
		$validate = Validator::make($request->all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this::find($request->get('id'));
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}


	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
}

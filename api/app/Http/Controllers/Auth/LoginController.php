<?php

namespace App\Http\Controllers\Auth;

include_once "SMSC.php";

use App\Http\Controllers\Controller;
use App\Http\Models\User\User;
use App\Http\Models\User\Profile;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('name', 'password');
		try {
			$user = User::where('name', $request->get('name'))->first();
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json('Неверные данные', 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json('Ошибка сервера', 500);
		}

		// all good so return the token
		$roles = $user->getRoleNames();
		return response()->json([
			'token' => $token, 
			'roles' => $roles[0], 
			'user' => [
				'id' => $user['id'],
				'name' => $user['name']
			],
		]);
	}

	public function authenticatePhone(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('phone', 'code', 'token');
		try {
			$profile = Profile::where([
				['phone', '=', $request->get('phone')],
				['sms_code', '=', $request->get('code')]
			])->first();
			if ($profile && $token = $request->get('token'))
			{
				$profile->token = $token;
				$profile->save();
				$user = User::where('id', $profile->id)->first();
				if (! $token = JWTAuth::fromUser($user)) {
					return response()->json('Неверные данные', 401);
				}
			}
			else
			{
				return response()->json('Не найден номер', 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json('Ошибка сервера', 500);
		}

		return response()->json([
			'token' => $token,
		]);
	}

	public function authenticateSms(Request $request)
	{
		if ($profile = (DB::table('profile')
			->select('profile.*')
			->where('phone', $request->get('phone'))
			// ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'profile.id')
			// ->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
			// ->where('roles.name', '=', 'driver')
			->first()))
		{
			$phone = $profile->phone;
			// $sms_id = "";
			// $sms_cnt = "";
			// $cost = "";
			// $balance = "";
			$smsCode = str_random(4);;
			$result = send_sms($phone, "Ваш код: ".$smsCode, 1);
			$profile = DB::table('profile')->where('phone', $phone)->update([
				'sms_code' => $smsCode
			]);
			if($result[0])
			{
				return response()->json("Сообщение успешно отправлено", 200);
			}
			{
				return response()->json("Ошибка! Сообщение не отправлено", 403);
			}
		}
		else
		{
			return response()->json("Не найден водитель с таким номером", 403);
		}
	}
}

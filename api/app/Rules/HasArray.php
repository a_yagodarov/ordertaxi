<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class HasArray implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_array($attribute))
        {
            foreach($attribute as $item)
            {
                if ($item[$value])
                    return true;
            }
        }
        else
            return false;
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Заполните это поле';
    }
}

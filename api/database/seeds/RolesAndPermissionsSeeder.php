<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.12.2017
 * Time: 22:10
 */
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run(){
        app()['cache']->forget('spatie.permission.cache');
        $role= Role::create(['name' => 'admin']);
        $role= Role::create(['name' => 'driver']);
        $role= Role::create(['name' => 'legal']);
    }
}
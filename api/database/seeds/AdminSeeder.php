<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Http\Models\User\User();
        $user->name = 'admin';
        $user->password = bcrypt('admin');
        $user->email = 'email@email.ru';
        $user->save();
        $user->assignRole('admin');
    }
}

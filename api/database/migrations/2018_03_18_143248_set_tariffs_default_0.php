<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetTariffsDefault0 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('request_price')->default(0)->nullable(false)->change();
            $table->integer('transfer')->default(0)->nullable(false)->change();
            $table->integer('order_cost')->default(0)->nullable(false)->change();
        });

        Schema::table('profile', function (Blueprint $table) {
            $table->integer('request_price')->default(0)->nullable(false)->change();
            $table->integer('transfer')->default(0)->nullable(false)->change();
            $table->integer('order_cost')->default(0)->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

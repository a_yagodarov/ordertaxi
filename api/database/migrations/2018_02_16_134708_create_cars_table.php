<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mark')->nullable();
            $table->string('model')->nullable();
            $table->string('car_number')->nullable();
            $table->string('color')->nullable();
            $table->integer('year')->nullable();
            $table->integer('passengers')->nullable();
            $table->integer('baggage')->nullable();
            $table->tinyInteger('baby_chair')->default(0);
            $table->tinyInteger('conditioner')->default(0);
            $table->tinyInteger('bank_card_payment')->default(0);
	        $table->integer('request_price')->nullable();
	        $table->integer('transfer')->nullable();
	        $table->integer('order_cost')->nullable();
            $table->text('comment')->nullable();
            $table->timestampsTZ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}

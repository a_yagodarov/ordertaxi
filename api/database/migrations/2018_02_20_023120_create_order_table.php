<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('datetime_order')->nullable();;
            $table->string('number');
            $table->string('from');
            $table->text('comment');
            $table->text('payment');
            $table->integer('price')->nullable();
            $table->integer('legal_id')->unsigned();
            $table->foreign('legal_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('car_id')->unsigned();
            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
            $table->string('status');
            $table->timestamps();
        });
        Schema::create('order_address', function(Blueprint $table){
            $table->integer('order_id')->unsigned();
            $table->string('where');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
        Schema::dropIfExists('order_address');
    }
}

<?php

return [

    'appNameIOS'     => [
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ],
    'appNameAndroid' => [
        'environment' =>'production',
        'apiKey'      =>'AAAAMrPM19M:APA91bGDx30GDGTnfJpz3dnD-RwuESc_Ne28B-NpNKfiU2-yztbErb5LmCLPvNZlbiTHLYT-fG0fdo7DJR2FuBxbKp8TvS3V6YH4bs4tLS_8PM9ov6yjLOi378PZwaIYpJzxZbiIbNbt',
        'service'     =>'gcm'
    ]

];
#Структура приложения:

папка api - backend
папка dist - скомпилированный front-end
src - исходники fronted
  
#Развертывание приложения:

# 1) Перейти в папку api

    прописать в файле .env данне для подключения к базе данных Mysql

# 2) Выполнить 
	
	php composer install

	php artisan migrate

	php artisan db:seed

# 3) Логин и пароль admin/admin

# 4) конфиг файлы в папке api/config:

    smsc - Настройки сервиса для отправки смс

    push-notification - Настройки облака для пуша сообщений через firebase.google.com
    
    yandex-map - Настройки для сервисов яндекс карт

#API
# 1) Авторизация

  POST запрос: домен/api/auth-phone
  
   **Headers**
  
  Authorization : Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI...
  
  **Body**:
  
  phone
  
  **Response:**
  
  {
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI..."
  }
  
  либо строка с ошибкой
# 2) Список заказов

  **POST запрос: домен/api/orders**
  
  **Headers**
  
  Authorization : Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI...
  
  **Response:**
  
`{
    "models": [
        {
            "id": 42,
            "datetime_order": "2018-03-15 07:00:00",
            "number": "222",
            "from": "Москва",
            "comment": "23",
            "payment": "noncash",
            "price": 1229,
            "legal_id": 4,
            "driver_id": null,
            "car_id": 3,
            "created_at": "2018-03-16 04:48:32",
            "updated_at": "2018-03-16 04:48:32",
            "status": 0,
            "driver_comment": "",
            "to": "Калининград",
            "legal_name": "ООО Рога и Копыта",
            "car_name": "Toyota Camry черный A555A3",
            "driver_name": "",
            "driver_phone": null
        },
],
    "count": 28
}`

# 3) Изменение статуса

  **POST запрос: домен/api/order-status**
  
  **Headers**
  
  Authorization : Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI...
  
  **Body:**
  
  id, status, driver_comment
  
  **Response:**
  
`{
    "id": [
        "Заказ не принадлежит водителю"
    ],
    "status": [
        "Невозможно изменить статус"
    ]
}`
# 4) Статусы
200 - все ок
401 - не авторизован
403 - не валидные данные(либо массив с парами поле - текст ошибки либо строка)
500 - ошибка сервера

